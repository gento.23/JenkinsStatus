#!/usr/bin/env python
import time
import serial
import requests
import sys
from requests.auth import HTTPBasicAuth

arduino=serial.Serial(sys.argv[4], 9600)

if(len(sys.argv) <= 4 ):
    print "Amount of arguments:"
    print len(sys.argv) - 1 
    print "Usage jenkins_status.py $URL $USER $PASSWORD $SERIAL_DEVICE"
    exit()
URL = sys.argv[1]
CREDS = (sys.argv[2], sys.argv[3])



def get_status():
    """Get the build status from Jenkins
    """
    r = requests.get(URL, auth=HTTPBasicAuth(*CREDS))
    return r.json


def parse_status(stat):
    """Parse the status returned by Jenkins
    """
    r = stat.get('result')
    if r == "SUCCESS":
        return "G"
    elif r == "FAILURE":
        return "R"
    else:
        return "Y"


while True:
    time.sleep(4)
    print "TEST"
    #arduino.write(parse_status(get_status()))
    arduino.write('G')
    time.sleep(5)
    exit()

